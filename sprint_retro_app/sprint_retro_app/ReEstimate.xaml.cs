﻿using sprint_retro_app.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sprint_retro_app
{
    /// <summary>
    /// Interaction logic for ReEstimate.xaml
    /// </summary>
    public partial class ReEstimate : Window
    {
        MainModelSingleton mms;
        ProjectInfo info;
        UserStory story_;

        public ReEstimate(UserStory story)
        {
            InitializeComponent();
            mms = MainModelSingleton.Instance;
            info = mms.Project;
            story_ = story;
            userStoryNameTb.Text = story_.StoryName;
        }

        private void OkayBtn_Click(object sender, RoutedEventArgs e)
        {
            info.Sprints.ElementAt(info.CurrentSprint - 1).UserStories.Find(c => c == story_).RelativeEstimates = int.Parse(reestimateTb.Text);
            this.Close();
        }
    }
}
