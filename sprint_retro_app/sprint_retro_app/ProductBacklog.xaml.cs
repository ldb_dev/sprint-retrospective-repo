﻿using sprint_retro_app.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sprint_retro_app
{
    /// <summary>
    /// Interaction logic for ProductBacklog.xaml
    /// </summary>
    public partial class ProductBacklog : Window
    {
        //class members
        ProjectInfo info;
        MainModelSingleton mms;
        Sprint sprint;
        //windows
        Menu menuWindow;
        Sprints sprintsWindow;
        //path
        string dir;
        
    
        //ctor
        public ProductBacklog()
        {
          
            dir = Environment.CurrentDirectory;
            InitializeComponent();
            mms = MainModelSingleton.Instance;
            info = mms.Project;
            prodBacklogLb.ItemsSource = info.Backlog.Where(c=>c.AssignedSprint == 0);
            addToSprintBtn.Content += $"({info.CurrentSprint})";
        }

        //Event Handling for Adding a user story to the product backlog
        private void AddUserStoryBtn_Click(object sender, RoutedEventArgs e)
        {
            if (storyNameTb.Text == "" || relativeEstTb.Text == "" || backlogTb.Text == "")
            {
                MessageBox.Show("You must enter data into all fields!");
                return;
            }
            UserStory us = new UserStory(storyNameTb.Text, backlogTb.Text, int.Parse(relativeEstTb.Text), 0);
            //add the user story to the singletons userstories list
            info.Backlog.Add(us);
            MessageBox.Show("(1) User Story Added to Backlog");
            prodBacklogLb.ItemsSource = info.Backlog.Where(c => c.AssignedSprint == 0);
            prodBacklogLb.Items.Refresh();

            //clear the fields
            backlogTb.Text = ""; relativeEstTb.Text = ""; storyNameTb.Text = "";
        }
       

        private void AddToSprintBtn_Click(object sender, RoutedEventArgs e)
        {
            //ensure they have selected a userstory
            if (prodBacklogLb.SelectedIndex == -1)
                MessageBox.Show("You must select an item to add to the sprint!");
            else
            {
                UserStory selectedItem = (UserStory)prodBacklogLb.SelectedItem;
                selectedItem.AssignedSprint = info.CurrentSprint;
                //add the selected item to the sprints list of info
                info.Sprints.ElementAt(info.CurrentSprint - 1).UserStories.Add(selectedItem);
                //indicate user story has been added
                MessageBox.Show("(1) User Story added to current Sprint\nRemoved from backlog!");
                //remove it from the backlog(as it is now in the sprint)
                info.Backlog.Remove(selectedItem);
                //refresh
                prodBacklogLb.ItemsSource = info.Backlog.Where(c => c.AssignedSprint == 0);
                prodBacklogLb.Items.Refresh();
            }

        }

        private void RemoveFromBacklogBtn_Click(object sender, RoutedEventArgs e)
        {
            //ensure they have selected a userstory
            if (prodBacklogLb.SelectedIndex == -1)
                MessageBox.Show("You must select an item to remove it!");
            else
            {
                var selectedItem = (UserStory)prodBacklogLb.SelectedItem;
                info.Backlog.Remove(selectedItem);
                prodBacklogLb.ItemsSource = info.Backlog.Where(c => c.AssignedSprint == 0);
                prodBacklogLb.Items.Refresh();
            }

        }

        private void GoToSprintsBtn_Click(object sender, RoutedEventArgs e)
        {
            sprintsWindow = new Sprints();
            this.Close();
            sprintsWindow.ShowDialog();
            mms.ProjectPath = $"{dir}\\{info.GroupName}.xml";
            mms.SaveProject();

        }

        private void GoToMenuBtn_Click(object sender, RoutedEventArgs e)
        {
            mms.ProjectPath = $"{dir}\\{info.GroupName}.xml";
            mms.SaveProject();

            menuWindow = new Menu();
            this.Close();
            menuWindow.ShowDialog();
        }
    }
}
