﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace sprint_retro_app.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("sprint_retro_app.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirm Password:.
        /// </summary>
        public static string confirmPw {
            get {
                return ResourceManager.GetString("confirmPw", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Legend.
        /// </summary>
        public static string graphLegend {
            get {
                return ResourceManager.GetString("graphLegend", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Story Progress:.
        /// </summary>
        public static string graphPercentComplete {
            get {
                return ResourceManager.GetString("graphPercentComplete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select User Story.
        /// </summary>
        public static string graphSelectStory {
            get {
                return ResourceManager.GetString("graphSelectStory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Predicted vs. Actual User Story Completion.
        /// </summary>
        public static string graphTitle {
            get {
                return ResourceManager.GetString("graphTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Days.
        /// </summary>
        public static string graphXaxis {
            get {
                return ResourceManager.GetString("graphXaxis", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Story Hours.
        /// </summary>
        public static string graphYaxis {
            get {
                return ResourceManager.GetString("graphYaxis", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Group Name:.
        /// </summary>
        public static string groupname {
            get {
                return ResourceManager.GetString("groupname", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Login:.
        /// </summary>
        public static string login {
            get {
                return ResourceManager.GetString("login", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Login.
        /// </summary>
        public static string loginHeader {
            get {
                return ResourceManager.GetString("loginHeader", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product Backlog.
        /// </summary>
        public static string menuBacklog {
            get {
                return ResourceManager.GetString("menuBacklog", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Graph Data.
        /// </summary>
        public static string menuCalculations {
            get {
                return ResourceManager.GetString("menuCalculations", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Logout.
        /// </summary>
        public static string menuLogout {
            get {
                return ResourceManager.GetString("menuLogout", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sprint Summary.
        /// </summary>
        public static string menuSummary {
            get {
                return ResourceManager.GetString("menuSummary", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Time Capture.
        /// </summary>
        public static string menuTime {
            get {
                return ResourceManager.GetString("menuTime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password:.
        /// </summary>
        public static string password {
            get {
                return ResourceManager.GetString("password", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Register.
        /// </summary>
        public static string register {
            get {
                return ResourceManager.GetString("register", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Register Account.
        /// </summary>
        public static string registerHeader {
            get {
                return ResourceManager.GetString("registerHeader", resourceCulture);
            }
        }
    }
}
