﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sprint_retro_app.Models
{
    public class GraphObjectUserStoryTimeEntry
    {
        public UserStory UserStory { get; set; }
        public List<TimeEntry> TimeEntries {get; set;}
    }
}
