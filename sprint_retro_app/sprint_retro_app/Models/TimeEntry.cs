﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sprint_retro_app.Models
{
    // will be xml deserializable and serializable
    public class TimeEntry
    {
        // what userstory is this timeentry for
        //public ProjectInfo Project { get; set; }
        //public Sprint Sprint { get; set; }
        public UserStory UserStory { get; set; }

        // actual entry data
        public double TimeTaken { get; set; }
        public double TotalTimeTaken { get; set; } = 0;
        public DateTime Date { get; set; } // will just be datetime.now
        public string User { get; set; }
    }
}
