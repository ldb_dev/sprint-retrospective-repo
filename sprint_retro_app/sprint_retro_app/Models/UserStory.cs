﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sprint_retro_app.Models
{
    public class UserStory
    {
        //properties
        //the actual user story
        public string StoryName { get; set; }
        public string StoryDesc { get; set; }
        //bool inidcator for later to determine if it's complete, or must be passed into next sprint
        public bool IsDone { get; set; } = false;
        //int to track relative estimates
        public int RelativeEstimates { get; set; }
        //int to indiciate which sprint this user story belongs to (0 if not assigned)
        public int AssignedSprint { get; set; }
        //Contributor
        public string Owner { get; set; }

        // constructor
        public UserStory() { }

        public UserStory(string storyname,string storydesc, int estimate, int assignedSprint)
        {
            StoryName = storyname;
            StoryDesc = storydesc;
            RelativeEstimates = estimate;
            AssignedSprint = assignedSprint;
        }

        public override string ToString()
        {
            string rtn = $"{StoryName}\nRelative Estimate: {RelativeEstimates}\nSprint: {(AssignedSprint == 0 ?  "N/A" : AssignedSprint.ToString())}";

            if (Owner != null)
                rtn += $"\nContributor: {Owner}";

            return rtn;
        }
    }
}
