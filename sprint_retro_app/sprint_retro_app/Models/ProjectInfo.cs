﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sprint_retro_app.Models
{
    public class ProjectInfo
    {
        public string ProjectName { get; set; }
        public string GroupName { get; set; }
        public string ProjectDesc { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int CurrentSprint { get; set; }
        public double HoursPerStoryPoint { get; set; }
        public int SprintVelocity { get; set; }
        public List<Sprint> Sprints { get; set; }        
        public List<UserStory> Backlog { get; set; } // on merge with master



        public List<string> Contributors { get; set; }
        private double _startingTotalEstimate;
        private double _currentRemainingEstimate;
        private double _velocity;

        //Methods
        void AddContributor(string name)
        {
            Contributors.Add(name);
        }
        string[] ListContributors()
        {
            return Contributors.ToArray();
        }

        //contructors
        public ProjectInfo(string name, string desc, DateTime Start, DateTime end)
        {
            ProjectName = name;
            ProjectDesc = desc;
            StartDate = Start;
            EndDate = end;

            Contributors = new List<string>();
            Sprints = new List<Sprint>();
            Backlog = new List<UserStory>();
        }

        public ProjectInfo()
        {

            //Sprints = new List<Sprint>();
            //Backlog = new List<UserStory>();
            //TimeEntries = new List<TimeEntry>();
        }
    }
}
