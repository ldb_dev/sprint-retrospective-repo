﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sprint_retro_app.Models
{
    public class Sprint
    {
        public int Number { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public List<UserStory> UserStories { get; set; }
        public List<TimeEntry> TimeEntries { get; set; }

        public Sprint()
        {
            UserStories = new List<UserStory>();
            TimeEntries = new List<TimeEntry>();
        }
    }
}
