﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace sprint_retro_app.Models
{
    public class MainModelSingleton
    {
        #region singleton
        private static MainModelSingleton instance = null;
        private static readonly object padlock = new object();



        private MainModelSingleton() { }

        //Singleton
        public static MainModelSingleton Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new MainModelSingleton();
                    }
                    return instance;
                }
            }
        }
        #endregion


        public ProjectInfo Project { get; set; }
        public string ProjectPath { get; set; }

        public void LoadProject()
        {
            var file = new StreamReader(ProjectPath);
            var xmlReader = new XmlSerializer(typeof(ProjectInfo));
            Project = ((ProjectInfo)xmlReader.Deserialize(file));
        }

        public void SaveProject()
        {
            var stream = new FileStream(ProjectPath, FileMode.Create, FileAccess.Write);
            var s = new System.Xml.Serialization.XmlSerializer(typeof(ProjectInfo));
            s.Serialize(stream, Project);
            stream.Close();
        }
    }
}
