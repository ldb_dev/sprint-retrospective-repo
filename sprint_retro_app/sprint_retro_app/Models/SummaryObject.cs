﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sprint_retro_app.Models
{
    public class SummaryObject
    {
        public string StoryName { get; set; }
        public string Owner { get; set; }
        public double StoryPoints { get; set; }
        public double HoursWorked { get; set; }
        public double PercentWorked { get; set; }
        public string Status { get; set; }
    }
}
