﻿using sprint_retro_app.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Serialization;

namespace sprint_retro_app
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //Members
        Menu menu;
        ProjectCreationWindow pcw;
        MainModelSingleton mms;
       
        string dir = Environment.CurrentDirectory;
        string path = "";

        public MainWindow()
        {
            InitializeComponent();
            mms =  MainModelSingleton.Instance;
        }

        private void LoginBtn_Click(object sender, RoutedEventArgs e)
        {
            

            path = $"{dir}\\{loginUsernameTb.Text}.xml";
            if (File.Exists(path))
            {
                mms.ProjectPath = path;
                mms.LoadProject();

                //LoadProjectData(loginUsernameTb.Text, dir);
                menu = new Menu();
                this.Close();
                menu.ShowDialog();
            }
            else
            {
                MessageBox.Show("You must register before logging in!");
            }
           
        }

        private void RegisterBtn_Click(object sender, RoutedEventArgs e)
        {
            pcw = new ProjectCreationWindow();
            pcw.ShowDialog();
        }

    }
}
