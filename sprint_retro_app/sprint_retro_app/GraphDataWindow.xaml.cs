﻿using sprint_retro_app.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sprint_retro_app
{
    /// <summary>
    /// Interaction logic for GraphData.xaml
    /// </summary>
    public partial class GraphDataWindow : Window
    {
        Menu menu;
        MainModelSingleton mms;
        UserStory selectedUserStory;
        GraphObjectUserStoryTimeEntry entryToGraph;
        public GraphDataWindow()
        {
            InitializeComponent();
            mms = MainModelSingleton.Instance;

            tbCurrentProject.Text = $"Project: {mms.Project.ProjectName}";
            tbCurrentSprint.Text = $"Sprint: {mms.Project.CurrentSprint}";

            tbXAxis.Text = $"Days Since Sprint Start ({mms.Project.Sprints.Last().StartDate.ToString("dddd, dd MMMM yyyy")})";

            cmbUserStories.Items.Clear();
            cmbUserStories.DisplayMemberPath = "StoryName";
            foreach (var story in mms.Project.Sprints.Last().UserStories)
            {     
                cmbUserStories.Items.Add(story);
            }
        }

        private void CmbUserStories_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedUserStory = (UserStory)cmbUserStories.SelectedItem;
            txtPredictedLine.Text = $"Predicted: {selectedUserStory.StoryName}";
            txtActualLine.Text = $"Actual: {selectedUserStory.StoryName}";
            GraphTheData();
            if (entryToGraph.UserStory.IsDone)
                tbProgress.Text = "Complete";
            else
                tbProgress.Text = $"Incomplete: {Math.Round((entryToGraph.TimeEntries.Last().TotalTimeTaken) / (entryToGraph.UserStory.RelativeEstimates * mms.Project.HoursPerStoryPoint) * 100, 2, MidpointRounding.AwayFromZero)} %";
            tbHoursWorked.Text = $"{(entryToGraph.TimeEntries.Last().TotalTimeTaken)}/{(entryToGraph.UserStory.RelativeEstimates * mms.Project.HoursPerStoryPoint)} Hours Worked";
        }

        #region graphstuff
        double wxmin;
        double wxmax;
        double wymin;
        double wymax;

        double xstep;
        double ystep;

        public void SetRanges()
        {
            wxmin = 0;
            var firstday = mms.Project.Sprints.Last().StartDate;
            var lastday = mms.Project.Sprints.Last().EndDate;
            if (lastday == DateTime.MinValue)
            {
                lastday = firstday.AddDays(mms.Project.SprintVelocity);
                mms.Project.Sprints.Last().EndDate = lastday;
            }
            foreach (var item in entryToGraph.TimeEntries)
            {
                if (item.Date > lastday)
                    lastday = item.Date;
            }
                          
            wxmax = (lastday - firstday).TotalDays;

            wymin = 0;
            wymax = 0;

            wymax = selectedUserStory.RelativeEstimates * mms.Project.HoursPerStoryPoint;
            if (entryToGraph.TimeEntries.Count > 0 
                && entryToGraph.TimeEntries?.OrderBy(x => x.TotalTimeTaken).Last().TotalTimeTaken > wymax)
                wymax = entryToGraph.TimeEntries.OrderBy(x => x.TotalTimeTaken).Last().TotalTimeTaken;

            xstep = 1;
            ystep = 1;
            wymax += ystep;
        }


        private void GraphTheData()
        {
            CreateGraphObject();
            SetRanges();            
            DrawGraph();
        }

        private void DrawGraph()
        {
            // remove previous graph stuff
            for (int i = canGraph.Children.Count - 1; i >= 4; i--) 
            {
                canGraph.Children.RemoveAt(i);
            }

            const double dmargin = 150;
            double dxmin = dmargin;
            double dxmax = canGraph.Width - dmargin;
            double dymin = dmargin;
            double dymax = canGraph.Height - dmargin;

            // Prepare the transformation matrices.
            PrepareTransformations(
                wxmin, wxmax, wymin, wymax,
                dxmin, dxmax, dymax, dymin);

            // Get the tic mark lengths.
            var p0 = DtoW(new Point(0, 0));
            var p1 = DtoW(new Point(5, 5));
            double xtic = p1.X - p0.X;
            double ytic = p1.Y - p0.Y;

            // Make the X axis.
            var xaxis_geom = new GeometryGroup();
            p0 = new Point(wxmin, wymin);
            p1 = new Point(wxmax, wymin);
            xaxis_geom.Children.Add(new LineGeometry(WtoD(p0), WtoD(p1)));

            for (double x = wxmin; x <= wxmax + 1 - xstep; x += xstep)
            {
                // Add the tic mark.
                var tic0 = WtoD(new Point(x, -ytic + wymin));
                var tic1 = WtoD(new Point(x, ytic + wymin));
                xaxis_geom.Children.Add(new LineGeometry(tic0, tic1));

                // Label the tic mark's X coordinate.
                DrawText(canGraph, x.ToString(),
                    new Point(tic0.X, tic0.Y + 5), 12,
                    HorizontalAlignment.Center,
                    VerticalAlignment.Top);
            }

            var xaxis_path = new System.Windows.Shapes.Path();
            xaxis_path.StrokeThickness = 5;
            xaxis_path.Stroke = Brushes.Black;
            xaxis_path.Data = xaxis_geom;

            canGraph.Children.Add(xaxis_path);

            // Make the Y axis.
            var yaxis_geom = new GeometryGroup();
            p0 = new Point(wxmin, wymin);
            p1 = new Point(wxmin, wymax);
            xaxis_geom.Children.Add(new LineGeometry(WtoD(p0), WtoD(p1)));

            for (double y = wymin; y <= wymax; y += ystep)
            {
                // Add the tic mark.
                var tic0 = WtoD(new Point(-xtic + wxmin, y));
                var tic1 = WtoD(new Point(xtic + wxmin, y));
                xaxis_geom.Children.Add(new LineGeometry(tic0, tic1));

                // Label the tic mark's Y coordinate.
                DrawText(canGraph, FormatNumber((long)y),
                    new Point(tic0.X, tic0.Y), 12,
                    HorizontalAlignment.Right,
                    VerticalAlignment.Center);
            }

            var yaxis_path = new System.Windows.Shapes.Path();
            yaxis_path.StrokeThickness = 5;
            yaxis_path.Stroke = Brushes.Black;
            yaxis_path.Data = yaxis_geom;

            canGraph.Children.Add(yaxis_path);

            // Make some data sets.
            Brush[] brushes = {Brushes.Green, Brushes.Red };

            var pointsPredicted = new PointCollection();
            var pointsActual = new PointCollection();
            pointsPredicted.Add(WtoD(new Point(0, 0)));
            pointsPredicted.Add(WtoD(new Point(wxmax, entryToGraph.UserStory.RelativeEstimates * mms.Project.HoursPerStoryPoint)));

            pointsActual.Add(WtoD(new Point(0, 0)));
            foreach (var item in entryToGraph.TimeEntries)
            {
                double x = (item.Date - mms.Project.StartDate).TotalDays;
                pointsActual.Add(WtoD(new Point(x, item.TotalTimeTaken)));
            }

            var polylinePredicted = new Polyline();
            polylinePredicted.StrokeThickness = 5;
            polylinePredicted.Stroke = brushes[0];
            polylinePredicted.Points = pointsPredicted;
            canGraph.Children.Add(polylinePredicted);

            var polylineActual = new Polyline();
            polylineActual.StrokeThickness = 5;
            polylineActual.Stroke = brushes[1];
            polylineActual.Points = pointsActual;
            canGraph.Children.Add(polylineActual);
        }

        private void CreateGraphObject()
        {
            // create the graphobject with time entries
            entryToGraph = new GraphObjectUserStoryTimeEntry();
            entryToGraph.TimeEntries = new List<TimeEntry>();
            entryToGraph.UserStory = selectedUserStory;
            double totalTimeTaken = 0;
            foreach (var timeentry in mms.Project.Sprints.Last().TimeEntries.OrderBy(x => x.Date))
            {
                if (timeentry.UserStory.StoryName == selectedUserStory.StoryName)
                {
                    totalTimeTaken += timeentry.TimeTaken;
                    entryToGraph.TimeEntries.Add(timeentry);
                    entryToGraph.TimeEntries.Last().TotalTimeTaken = totalTimeTaken;
                }
            }
        }

        #endregion

        #region transformation
        // Prepare values for perform transformations.
        private Matrix WtoDMatrix, DtoWMatrix;
        private void PrepareTransformations(
            double wxmin, double wxmax, double wymin, double wymax,
            double dxmin, double dxmax, double dymin, double dymax)
        {
            // Make WtoD.
            WtoDMatrix = Matrix.Identity;
            WtoDMatrix.Translate(-wxmin, -wymin);

            double xscale = (dxmax - dxmin) / (wxmax - wxmin);
            double yscale = (dymax - dymin) / (wymax - wymin);
            WtoDMatrix.Scale(xscale, yscale);

            WtoDMatrix.Translate(dxmin, dymin);

            // Make DtoW.
            DtoWMatrix = WtoDMatrix;
            DtoWMatrix.Invert();
        }

        // Transform a point from world to device coordinates.
        private Point WtoD(Point point)
        {
            return WtoDMatrix.Transform(point);
        }

        // Transform a point from device to world coordinates.
        private Point DtoW(Point point)
        {
            return DtoWMatrix.Transform(point);
        }
        #endregion

        // Position a label at the indicated point.
        private void DrawText(Canvas can, string text, Point location,
            double font_size,
            HorizontalAlignment halign, VerticalAlignment valign)
        {
            // Make the label.
            var label = new Label();
            label.Content = text;
            label.FontSize = font_size;
            can.Children.Add(label);

            // Position the label.
            label.Measure(new Size(double.MaxValue, double.MaxValue));

            double x = location.X;
            if (halign == HorizontalAlignment.Center)
                x -= label.DesiredSize.Width / 2;
            else if (halign == HorizontalAlignment.Right)
                x -= label.DesiredSize.Width;
            Canvas.SetLeft(label, x);

            double y = location.Y;
            if (valign == VerticalAlignment.Center)
                y -= label.DesiredSize.Height / 2;
            else if (valign == VerticalAlignment.Bottom)
                y -= label.DesiredSize.Height;
            Canvas.SetTop(label, y);
        }

        // Formatting stuff
        public string FormatNumber(long num)
        {
            num = MaxThreeSignificantDigits(num);

            if (num >= 100000000)
                return (num / 1000000D).ToString("0.#M");
            if (num >= 1000000)
                return (num / 1000000D).ToString("0.##M");
            if (num >= 100000)
                return (num / 1000D).ToString("0k");
            if (num >= 100000)
                return (num / 1000D).ToString("0.#k");
            if (num >= 1000)
                return (num / 1000D).ToString("0.##k");
            return num.ToString("#,0");
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            menu = new Menu();
            this.Close();
            menu.ShowDialog();
        }

        public long MaxThreeSignificantDigits(long x)
        {
            var i = (int)Math.Log10(x);
            i = Math.Max(0, i - 2);
            i = (int)Math.Pow(10, i);
            return x / i * i;
        }
    }
}
