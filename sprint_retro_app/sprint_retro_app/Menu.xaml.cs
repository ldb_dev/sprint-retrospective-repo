﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sprint_retro_app
{
    /// <summary>
    /// Interaction logic for Menu.xaml
    /// </summary>
    public partial class Menu : Window
    {
        //Members
        ProductBacklog backlog;
        MainWindow login;
        TimeCapture timeCapture;
        GraphDataWindow dataWindow;
        SprintProgressGrid summaryWindow;
        //GraphWindow Graph //For when it gets made


        public Menu()
        {
            InitializeComponent();
            ProjectName.Content = Models.MainModelSingleton.Instance.Project.ProjectName;
            //test
        }

        private void BtnProdBacklog_Click(object sender, RoutedEventArgs e)
        {
            backlog = new ProductBacklog();
            this.Close();
            backlog.ShowDialog();
        }

        private void BtnTime_Click(object sender, RoutedEventArgs e)
        {
            timeCapture = new TimeCapture();
            this.Close();
            timeCapture.ShowDialog();
        }

        // will display the data in some logical way
        private void BtnCalculations_Click(object sender, RoutedEventArgs e)
        {
            dataWindow = new GraphDataWindow();
            this.Close();
            dataWindow.ShowDialog();
        }

        private void BtnSummary_Click(object sender, RoutedEventArgs e)
        {
            summaryWindow = new SprintProgressGrid();
            this.Close();
            summaryWindow.ShowDialog();
        }

        private void BtnLogout_Click(object sender, RoutedEventArgs e)
        {
            login = new MainWindow();
            this.Close();
            login.ShowDialog();
        }
    }
}
