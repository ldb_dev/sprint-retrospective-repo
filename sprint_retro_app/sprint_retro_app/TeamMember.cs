﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sprint_retro_app
{
    public class TeamMember
    {
        string Name { get; set; }
        //Current hours member is exp-ected to do this sprint
        double CurrentEstimatedHours { get; set; }

        //Current hours member has done this sprint
        double CurrentActualHours { get; set; }

        //Overall hours user has been estimated to of needed to work, including current
        double OverallEstimatedHours { get; set; }

        //Overall hours user has worked, Including current
        double OverallActualHours { get; set; }
    }
}
