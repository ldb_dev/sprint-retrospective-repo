﻿using sprint_retro_app.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sprint_retro_app.ViewModels
{
    public class SummaryViewModel : ViewModelBase
    {
        MainModelSingleton mms;

        public ObservableCollection<SummaryObject> StoryData { get; set; }

        public string Project { get; set; } = "test";
        public string Sprint { get; set; }

        public SummaryViewModel()
        {
            mms = MainModelSingleton.Instance;
            Project = mms.Project.GroupName;
            Sprint = mms.Project.CurrentSprint.ToString();
            StoryData = new ObservableCollection<SummaryObject>();
            SetStoryData();
        }

        private void SetStoryData()
        {
            // create the graphobject with time entries
            //entryToGraph = new GraphObjectUserStoryTimeEntry();
            //entryToGraph.TimeEntries = new List<TimeEntry>();
            //entryToGraph.UserStory = selectedUserStory;

            

            foreach (var story in mms.Project.Sprints.Last().UserStories)
            {
                var summaryObject = new SummaryObject();
                summaryObject.StoryName = story.StoryName;
                summaryObject.Owner = story.Owner;
                summaryObject.StoryPoints = story.RelativeEstimates;
                summaryObject.HoursWorked = CalculateHoursWorked(story);
                summaryObject.PercentWorked = summaryObject.HoursWorked / (mms.Project.HoursPerStoryPoint * story.RelativeEstimates) * 100;
                if (story.IsDone)
                    summaryObject.Status = "Complete";
                else
                    summaryObject.Status = "Incomplete";
                StoryData.Add(summaryObject);
            }
            var xx = 5;
        }

        private double CalculateHoursWorked(UserStory story)
        {
            double totalTimeTaken = 0;
            foreach (var timeentry in mms.Project.Sprints.Last().TimeEntries.OrderBy(x => x.Date))
            {
                if (timeentry.UserStory.StoryName == story.StoryName)
                {
                    totalTimeTaken += timeentry.TimeTaken;
                }
            }
            return totalTimeTaken;
        }
    }
}
