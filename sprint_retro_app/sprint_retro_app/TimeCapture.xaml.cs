﻿using Microsoft.Win32;
using sprint_retro_app.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace sprint_retro_app
{
    /// <summary>
    /// Interaction logic for TimeCapture.xaml
    /// </summary>
    public partial class TimeCapture : Window
    {
        Menu menu;
        MainModelSingleton mms;
        string FileName = "project1test";
        string dir = Environment.CurrentDirectory;
        string path = "";
        string selectedUser = "";
        

        public TimeCapture()
        {
            InitializeComponent();
            mms = MainModelSingleton.Instance;
            path = $"{dir}\\{FileName}.xml";
            //SaveTestProject();
            LoadTestProject();
        }

        public void SaveTestProject()
        {
            ProjectInfo pi = new ProjectInfo("testname", "testdesc", DateTime.Now, DateTime.Now.AddDays(5));

            pi.Contributors.Add("Hur");
            pi.Contributors.Add("Jacob");
            pi.Contributors.Add("Daniel");

            Sprint sp = new Sprint();
            sp.StartDate = DateTime.Now;
            sp.EndDate = DateTime.Now.AddDays(5);

            UserStory us1 = new UserStory("gather requirements", "gather some requirements and stuff", 5,1);
            UserStory us2 = new UserStory("build project", "spend some time to build da project", 5,1);
            UserStory us3 = new UserStory("test project", "test the project cant be delivring broken code",5, 1);
            UserStory us4 = new UserStory("deploy project", "deploy so we can finally get paid and go home",5, 1);

            sp.UserStories.Add(us1);
            sp.UserStories.Add(us2);
            sp.UserStories.Add(us3);
            sp.UserStories.Add(us4);

            pi.Sprints.Add(sp);

            var stream = new FileStream($"{dir}\\{FileName}.xml", FileMode.Create, FileAccess.Write);
            var s = new System.Xml.Serialization.XmlSerializer(typeof(ProjectInfo));
            s.Serialize(stream, pi);
            stream.Close();
        }

        public void LoadTestProject()
        {
            mms.LoadProject();
            if (mms.Project != null && mms.Project.Sprints != null)
            {
                tbHeaderProjectInfo.Text = $"Enter Time Capture Data for Project {mms.Project.ProjectName} - Sprint {mms.Project.Sprints.Last().Number}";
                LoadTasks();
            }                
            else
                tbHeaderProjectInfo.Text = $"Please Load a project with an existing sprint";
            
        }

        private void LoadTasks()
        {
            cmbTasks.Items.Clear();
            cmbTasks.DisplayMemberPath = "StoryName";
            foreach (var task in mms.Project.Sprints.Last().UserStories)
            {
                if (!task.IsDone)
                    cmbTasks.Items.Add(task);
            }
            cmbTasks.Text = "-- Select Task --";

            cmbUsers.Items.Clear();
            foreach (var contr in mms.Project.Contributors)
            {
                cmbUsers.Items.Add(contr);
            }
            cmbUsers.Text = "-- Select User --";
        }

        private void CmbTasks_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cmb = (ComboBox)sender;
            if (cmb.SelectedIndex == -1)
                return;

            var selectedTask = (UserStory)cmbTasks.SelectedItem;

            tbSelectedTaskName.Text = selectedTask.StoryName;
            tbEstCompletionTime.Text = $"{selectedTask.RelativeEstimates * mms.Project.HoursPerStoryPoint} hour(s)";
            tbOwner.Text = selectedTask.Owner;
            tbTaskDescription.Text = selectedTask.StoryDesc;
            AllowSubmitButton();
        }

        private void CmbUsers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cmb = (ComboBox)sender;
            if (cmb.SelectedIndex == -1)
                return;

            selectedUser = (string)cmbUsers.SelectedItem;

            AllowSubmitButton();
        }

        private void BtnSubmit_Click(object sender, RoutedEventArgs e)
        {
            if (chkFinished.IsChecked == true)
                ((UserStory)cmbTasks.SelectedItem).IsDone = true;
            mms.Project.Sprints.Last().TimeEntries.Add(new TimeEntry()
            {
                Date = DateTime.Now,
                TimeTaken = double.Parse(txtHours.Text),
                UserStory = (UserStory)cmbTasks.SelectedItem
            });
            txtHours.Text = null;
            LoadTasks();
            //cmbTasks.SelectedItem = null;
            //cmbTasks.Text = "-- Select Task --";
            //cmbUsers.SelectedItem = null;
            //cmbUsers.Text = "-- Select User --";

            tbSelectedTaskName.Text = "";
            tbEstCompletionTime.Text = "";
            tbTaskDescription.Text = "";
            tbOwner.Text = "";
            chkFinished.IsChecked = false;

            mms.SaveProject();
        }

        private void TxtHours_TextChanged(object sender, TextChangedEventArgs e)
        {
            AllowSubmitButton();
        }

        private void AllowSubmitButton()
        {
            double hours;
            if (double.TryParse(txtHours.Text, out hours) 
                && cmbTasks.SelectedItem != null 
                && !string.IsNullOrWhiteSpace(selectedUser))
                btnSubmit.IsEnabled = true;
            else
                btnSubmit.IsEnabled = false;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            menu = new Menu();
            this.Close();
            menu.ShowDialog();
        }
    }
}
