﻿using sprint_retro_app.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;

namespace sprint_retro_app
{
    /// <summary>
    /// Interaction logic for ProjectCreationWindow.xaml
    /// </summary>
    public partial class ProjectCreationWindow : Window
    {
        MainModelSingleton mms;
        ProjectInfo info;
        List<string> contributorsList = new List<string>();

        string dir;
        
        public ProjectCreationWindow()
        {
            mms = MainModelSingleton.Instance;
            info = new ProjectInfo();
            dir = Environment.CurrentDirectory;
            InitializeComponent();
        }

        private void ContributorAddBtn_Click(object sender, RoutedEventArgs e)
        {
            if (contributorTb.Text != "")
            {
                contributorsList.Add(contributorTb.Text);
                contributorTb.Text = "";
                MessageBox.Show("Added Contributor");
            }
            else
            {
                MessageBox.Show("You must enter a contributor name.");
            }
        }

        private void CreateDoneBtn_Click(object sender, RoutedEventArgs e)
        {
            if (groupNameTb.Text == "" || NameBox.Text == "" || DescBox.Text == "" || StartDate.SelectedDate == null
                || EndDate.SelectedDate == null || hpspTb.Text == "" || contributorsList.Count < 1)
            {
                MessageBox.Show("YOU MUST ENTER DATA INTO ALL FIELDS");
            }
            else
            {
                mms.ProjectPath = $"{dir}\\{groupNameTb.Text}.xml";

                if (!File.Exists(mms.ProjectPath))
                {
                    try
                    {

                        //test user stories
                        List<UserStory> ulist = new List<UserStory>()
                        {
                            new UserStory("StoryName1","StoryDesc1", 5, 0),
                            new UserStory("StoryName2","StoryDesc2", 5, 0),
                            new UserStory("StoryName3","StoryDesc3", 5, 0),
                            new UserStory("StoryName4","StoryDesc4", 5, 0),
                            new UserStory("StoryName5","StoryDesc5", 5, 0),
                            new UserStory("StoryName6","StoryDesc6", 5, 0)
                        };


                        info.GroupName = groupNameTb.Text;
                        info.ProjectName = NameBox.Text;
                        info.ProjectDesc = DescBox.Text;
                        info.StartDate = (DateTime)StartDate.SelectedDate;
                        info.EndDate = (DateTime)EndDate.SelectedDate;
                        info.HoursPerStoryPoint = Double.Parse(hpspTb.Text);
                        info.SprintVelocity = int.Parse(sprintVelocityTb.Text);
                        info.Contributors = contributorsList;
                        info.CurrentSprint = 1;
                        info.Backlog = ulist;
                        info.Sprints = new List<Sprint>();
                        Sprint startSprint = new Sprint();
                        startSprint.StartDate = info.StartDate;
                        startSprint.EndDate = startSprint.StartDate.AddDays(7 * info.SprintVelocity);
                        info.Sprints.Add(startSprint);
                        //save to singleton
                        mms.Project = info;

                        mms.SaveProject();
                        this.Close();
                    }catch(Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Group name has already been taken!");
                }
            }
        }

      

    }
}
