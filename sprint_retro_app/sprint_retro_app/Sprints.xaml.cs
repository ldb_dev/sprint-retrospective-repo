﻿using sprint_retro_app.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sprint_retro_app
{
    /// <summary>
    /// Interaction logic for Sprints.xaml
    /// </summary>
    public partial class Sprints : Window
    {
        //members
        MainModelSingleton mms;
        ProductBacklog backlog;
        ReEstimate ree;
        ProjectInfo info;
        string dir;

        public Sprints()
        {
            mms = MainModelSingleton.Instance;
            info = mms.Project;
            dir = Environment.CurrentDirectory;
           
            InitializeComponent();
            cmbContribs.ItemsSource = info.Contributors;
            currSprintlbl.Content += $" {info.CurrentSprint}";
            startDate.SelectedDate = info.Sprints.ElementAt(info.CurrentSprint - 1).StartDate;
            endDate.SelectedDate = info.Sprints.ElementAt(info.CurrentSprint - 1).EndDate;
            sprintUserStoriesLb.ItemsSource = info.Sprints.ElementAt(info.CurrentSprint - 1).UserStories.Where(u => u.IsDone == false);
        }



        private void RemoveItemFromSprint_Click(object sender, RoutedEventArgs e)
        {
            if (sprintUserStoriesLb.SelectedIndex == -1)
                MessageBox.Show("You must select an item to remove it.");
            else
            {
                //if the user clicks yes, remove the item
                if (!(MessageBox.Show("Remove Item From Sprint?\nWarning! It can not be recovered!", "Question", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No))
                {
                    var selectedItem = (UserStory)sprintUserStoriesLb.SelectedItem;
                    info.Sprints.ElementAt(info.CurrentSprint - 1).UserStories.Remove(selectedItem);
                    sprintUserStoriesLb.ItemsSource = info.Sprints.ElementAt(info.CurrentSprint - 1).UserStories;
                    sprintUserStoriesLb.Items.Refresh();
                }

            }
        }

        private void CompleteSprintBtn_Click(object sender, RoutedEventArgs e)
        {
            backlog = new ProductBacklog();
            if (sprintUserStoriesLb.Items.Count > 0)
            {

                MessageBox.Show("Moving unfinished user stories to next sprint");
                //update the current sprint to the next 
                endDate.SelectedDate = DateTime.Today;
                info.Sprints.ElementAt(info.CurrentSprint - 1).EndDate = DateTime.Today;
                //make the next sprint
                Sprint nextSprint = new Sprint();
                nextSprint.StartDate = (DateTime)endDate.SelectedDate;
                nextSprint.EndDate = nextSprint.StartDate.AddDays(7 * info.SprintVelocity);
                
                foreach(UserStory story in sprintUserStoriesLb.Items)
                {
                    ree = new ReEstimate(story);
                    ree.ShowDialog();
                    story.AssignedSprint += 1;
                    nextSprint.UserStories.Add(story);
                }
                info.Sprints.Add(nextSprint);
                info.CurrentSprint += 1;
                mms.ProjectPath = $"{dir}\\{info.GroupName}.xml";
                mms.SaveProject();
                this.Close();
                    
                backlog.ShowDialog();                    
                
            }
            else
            {
                //update the current sprint to the next 
                endDate.SelectedDate = DateTime.Today;
                info.Sprints.ElementAt(info.CurrentSprint - 1).EndDate = DateTime.Today;
                info.CurrentSprint += 1;
                Sprint nextSprint = new Sprint();
                nextSprint.StartDate = (DateTime)endDate.SelectedDate;
                nextSprint.EndDate = nextSprint.StartDate.AddDays(7 * info.SprintVelocity);
               
                info.Sprints.Add(nextSprint);


                mms.ProjectPath = $"{dir}\\{info.GroupName}.xml";
                mms.SaveProject();
                this.Close();
                
                backlog.ShowDialog();                
            }
        }

        private void BackToMenu_Click(object sender, RoutedEventArgs e)
        {
            mms.ProjectPath = $"{dir}\\{info.GroupName}.xml";
            mms.SaveProject();
            backlog = new ProductBacklog();
            this.Close();
            backlog.ShowDialog();
        }


        private void AssignContributor_Click(object sender, RoutedEventArgs e)
        {
            if (cmbContribs.SelectedIndex == -1 || sprintUserStoriesLb.SelectedIndex == -1)
                MessageBox.Show("Please select both a story and contributor");
            else
                ((UserStory)sprintUserStoriesLb.SelectedItem).Owner = ((string)cmbContribs.SelectedItem);
            
            sprintUserStoriesLb.ItemsSource = info.Sprints.ElementAt(info.CurrentSprint - 1).UserStories.Where(c => c.IsDone == false);
            sprintUserStoriesLb.Items.Refresh();
        }

       
    }
}
